from rest_framework.pagination import CursorPagination


class MyLimitOffsetPagination(CursorPagination):
    page_size = 5
    ordering = 'name'  # if we dont write this, it will throw error: Cannot resolve keyword 'created' into field. Choices are: id, name, roll
